/*
 * AIDMCondition.h
 *
 *  Created on: Dec 24, 2015
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_AIDMCONDITION_H_
#define INCLUDE_AIDMCONDITION_H_

enum AIDMCondComparison {TRUE, EQUAL, NOT, AND, OR,};
#include "API/AIDMFunctionCall.h"

class AIDMCondition
{
public:
	AIDMCondition* _cond1;
	AIDMFunctionCall* _condF1;
	AIDMCondComparison comparison;
	AIDMFunctionCall* _condF2;
	AIDMCondition* _cond2;

	bool Check();
};

#endif /* INCLUDE_AIDMCONDITION_H_ */
