/*
 * AIDMClass.cpp
 *
 *  Created on: Jan 5, 2016
 *      Author: Steffen Kremer
 */

#include <AIDM/MISC/AIDMMisc.h>
#include "AIDMClass.h"

AIDMClass::AIDMClass(std::string classFile)
{
	this->_classFile = classFile;
	this->_className = "";
	this->_startGoal = 0;
	this->Interpret();
}

bool AIDMClass::checkIfActionExists(std::string actionName)
{
	for(unsigned int uiCurrentAction = 0;uiCurrentAction<this->_classActions.size();uiCurrentAction++)
	{
		if (this->_classActions[uiCurrentAction]->_name==actionName)
		{
			return true;
		}
	}
	return false;
}

void AIDMClass::Interpret(void)
{
	bool bIntInClass = false;
	bool bIntInCommentShort = false;
	bool bIntInCommentLong = false;
	//bool bIntInSlash
	bool bIntInAction = false;
	bool bIntInCondition = false;

	bool bIntInsString = false;

	unsigned int uiLastNLChar = 0;
	unsigned int uiCurrentLine = 1;
	std::ifstream in(this->_classFile.c_str(), std::ios::in | std::ios::binary);
	if (!in.good())
	{
		AIDMError(2,0,"Cant open File!");
	}
	std::string contents;
	if (in)
	{
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
	}
	//std::cout << contents;
	for (unsigned int uiCurrentChar=0; uiCurrentChar<contents.length(); ++uiCurrentChar)
	{
		switch(contents.at(uiCurrentChar))
		{
		case '/':
			if (bIntInsString)
			{
				break;
			}
			if (bIntInCommentLong&&contents.at(uiCurrentChar-1)=='*')
			{
				//std::cout << "Comment Stop long in " << uiCurrentLine << std::endl;
				bIntInCommentLong = false;
				break;
			}
			if (!bIntInCommentLong&&contents.at(uiCurrentChar+1)=='*')
			{
				//std::cout << "Comment Start long in " << uiCurrentLine << std::endl;
				uiCurrentChar++;
				bIntInCommentLong = true;
				break;
			}
			if (!bIntInCommentShort&&contents.at(uiCurrentChar+1)=='/')
			{
				//std::cout << "Comment Short in " << uiCurrentLine << std::endl;
				uiCurrentChar++;
				bIntInCommentShort = true;
				break;
			}
		break;
		case '\n':
			uiLastNLChar=uiCurrentChar;
			uiCurrentLine++;
			if (bIntInsString)
			{
				AIDMError(1,uiCurrentLine,"INTERROR: NEW LINE IN STRING");
			}
			if (bIntInCommentShort)
			{
				bIntInCommentShort = false;
				break;
			}
		break;
		case '"':
			if (!bIntInCommentShort&&!bIntInCommentLong)
			{
				bIntInsString = !bIntInsString;
			}
		break;
		case '\\':
			if (!bIntInsString)
			{
				AIDMError(1,uiCurrentLine,"INTERROR: ESCAPE OUT OF STRING");
			}
		break;
		case 'n':
			if (!bIntInCommentShort&&!bIntInCommentLong&&contents.at(uiCurrentChar+1)=='e'&&contents.at(uiCurrentChar+2)=='w'&&contents.at(uiCurrentChar+3)==' ')
			{
				uiCurrentChar+=4;
				if (contents.substr(uiCurrentChar,6)=="Class:")
				{
					if (this->_className!=""||bIntInClass)
					{
						AIDMError(1,uiCurrentLine,"INTERROR: DOUBLE CLASS DEFINITION");
					}
					uiCurrentChar+=6;
					unsigned int max = uiCurrentChar;
					for (unsigned int i=uiCurrentChar;i<contents.length();i++)
					{
						uiCurrentChar++;
						if (contents.at(i)=='\n'||contents.at(i)==' ')
						{
							if (contents.at(i)=='\n')
								uiLastNLChar=uiCurrentChar;
							//std::cout << "Found Base Class: " << this->_className << std::endl;
							break;
						}
						else
						{
							this->_className += contents.at(i);
						}
						if (i-max>=64)
						{
							AIDMError(1,uiCurrentLine,"INTERROR: CLASS NAME TOO LONG");
						}
					}
					if (!AIDMMiscCheckLegalStr(this->_className))
					{
						AIDMError(1,uiCurrentLine,"INTERROR: ILLEGAL CLASS NAME");
					}
					bIntInClass = true;
				}
				else if (!bIntInClass)
				{
					AIDMError(1,uiCurrentLine,"INTERROR: NO CLASS (1)");
				}
				else if (bIntInAction)
				{
					AIDMError(1,uiCurrentLine,"INTERROR: ONE ACTION ALREADY OPEN");
				}
				else if (bIntInCondition)
				{
					AIDMError(1,uiCurrentLine,"INTERROR: ONE CONDITION ALREADY OPEN");
				}
				else if (contents.substr(uiCurrentChar,7)=="Action:")
				{
					uiCurrentChar+=7;
					unsigned int max = uiCurrentChar;
					std::string actionName = "";
					for (unsigned int i=uiCurrentChar;i<contents.length();i++)
					{
						uiCurrentChar=i;
						if (contents.at(i)=='\n'||contents.at(i)==' ')
						{
							if (contents.at(i)=='\n')
								uiLastNLChar=uiCurrentChar;
							std::cout << uiCurrentLine << "Found Action: " << actionName << std::endl;
							break;
						}
						else
						{
							actionName += contents.at(i);
						}
						if (i-max>=64)
						{
							AIDMError(1,uiCurrentLine,"INTERROR: ACTION NAME TOO LONG");
						}
					}
					if (!AIDMMiscCheckLegalStr(actionName))
					{
						AIDMError(1,uiCurrentLine,"INTERROR: ILLEGAL ACTION NAME");
					}

					this->_classActions.insert(this->_classActions.end(),new AIDMAction(actionName));
					bIntInAction=true;
				}
				else if (contents.substr(uiCurrentChar,10)=="Condition:")
				{
					uiCurrentChar+=10;
					unsigned int max = uiCurrentChar;
					std::string conditionName = "";
					for (unsigned int i=uiCurrentChar;i<contents.length();i++)
					{
						uiCurrentChar=i;
						if (contents.at(i)=='\n'||contents.at(i)==' ')
						{
							if (contents.at(i)=='\n')
								uiLastNLChar=uiCurrentChar;
							std::cout << uiCurrentLine << "Found Condition: " << conditionName << std::endl;
							break;
						}
						else
						{
							conditionName += contents.at(i);
						}
						if (i-max>=64)
						{
							AIDMError(1,uiCurrentLine,"INTERROR: CONDITION NAME TOO LONG");
						}
					}
					if (!AIDMMiscCheckLegalStr(conditionName))
					{
						AIDMError(1,uiCurrentLine,"INTERROR: ILLEGAL ACTION NAME");
					}

					//this->_classActions.insert(this->_classActions.end(),new AIDMAction(actionName));
					bIntInCondition=true;
				}

			}
		break;
		case 9:
			if (uiLastNLChar==uiCurrentChar-1&&(!bIntInCommentShort&&!bIntInCommentLong))
			{
				if (!bIntInClass)
				{
					AIDMError(1,uiCurrentLine,"INTERROR: NO CLASS (0)");
				}
				if (bIntInAction||bIntInCondition)
				{
					uiCurrentChar++;
					for (unsigned int i=uiCurrentChar;i<contents.length();i++)
					{
						uiCurrentChar = i;
						if (contents.at(uiCurrentChar)==10)
						{
							std::cout << std::endl;
							uiLastNLChar=uiCurrentChar;
							uiCurrentLine++;
							break;
						}
						else
						{
							std::cout << contents.at(uiCurrentChar);
						}
					}
				}
			}
		break;
		case 'e':
			if (!bIntInsString&&!bIntInCommentShort&&!bIntInCommentLong&&contents.at(uiCurrentChar+1)=='n'&&contents.at(uiCurrentChar+2)=='d')
			{
				uiCurrentChar+=2;
				if (contents.length()>=uiCurrentChar+2)
				{
					uiCurrentChar++;
					if (!(contents.at(uiCurrentChar)=='\n'||contents.at(uiCurrentChar)==' '))
					{
						AIDMError(1,uiCurrentLine,"INTERROR: STATEMENT BEHIND END");
					}
				}
				if (bIntInAction)
				{
					bIntInAction = false;
					std::cout << uiCurrentLine << "Closed Action" << std::endl;
				}
				else if (bIntInCondition)
				{
					bIntInCondition = false;
					std::cout << uiCurrentLine << "Closed Condition" << std::endl;
				}
				else if (bIntInClass)
				{
					bIntInClass = false;
					std::cout << uiCurrentLine << "Closed Class" << std::endl;
				}
				else
				{
					AIDMError(1,uiCurrentLine,"INTERROR: NO OPEN ACTION/CONDITION/CLASS");
				}
			}
		break;
		//default:
			//std::cout << "";
		}
	}
	if (bIntInClass||bIntInCondition||bIntInAction)
	{
		AIDMError(1,uiCurrentLine,"INTERROR: OPEN ACTION/CONDITION/CLASS (0)");
	}
	std::cout << "Lines processed: " << uiCurrentLine << std::endl;
}
