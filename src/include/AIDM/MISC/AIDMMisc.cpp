/*
 * AIDMMisc.cpp
 *
 *  Created on: Jan 8, 2016
 *      Author: Steffen Kremer
 */

#include "AIDMMisc.h"
#include <iostream>

bool AIDMMiscCheckLegalChar(unsigned char cC)
{
	if (!((cC>=65&&cC<=90)||(cC>=97&&cC<=122)||(cC>=48&&cC<=57)))
	{
		return false;
	}
	return true;
}

bool AIDMMiscCheckLegalStr(std::string in)
{
	char cC;
	if (in.length()==0)
	{
		return false;
	}
	for (unsigned int uiCurrentChar=0; uiCurrentChar<in.length(); ++uiCurrentChar)
	{
		cC = in.at(uiCurrentChar);
		if (!AIDMMiscCheckLegalChar(cC))
		{
			return false;
		}
	}
	return true;
}

std::string AIDMMiscClearInterText(std::string in)
{
	std::string retStr = "";
	bool bIntInsString = false;
	for (unsigned int uiCurrentChar=0; uiCurrentChar<in.length(); ++uiCurrentChar)
	{
		if (in.at(uiCurrentChar)=='"')
		{
			bIntInsString = !bIntInsString;
			retStr += '"';
		}
		else if (in.at(uiCurrentChar)==9)
		{
			if (bIntInsString)
			{
				retStr += (char)9;
			}
			else
			{
				continue;
			}
		}
		else if (in.at(uiCurrentChar)==' ')
		{
			if (bIntInsString)
			{
				retStr += ' ';
			}
			else
			{
				continue;
			}
		}
		else if (in.at(uiCurrentChar)==10)
		{
			continue;
		}
		else
		{
			retStr += in.at(uiCurrentChar);
		}
	}
	if (bIntInsString)
	{
		return "";
	}
	return retStr;
}


