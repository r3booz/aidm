/*
 * AIDMError.cpp
 *
 *  Created on: Jan 8, 2016
 *      Author: Steffen Kremer
 */

#include "AIDMError.h"

void AIDMError(int errorNR, unsigned int line, std::string errorInfo)
{
	if (line>0)
	{
		std::cout << "AIDM Error " << errorNR << "@" << line << ": " << errorInfo << std::endl;
	}
	else
	{
		std::cout << "AIDM Error " << errorNR << ": " << errorInfo << std::endl;
	}
	std::exit(errorNR);
}

void AIDMWarning(int errorNR, unsigned int line, std::string errorInfo)
{
	std::cout << "AIDM Warning " << errorNR << "@" << line << ": " << errorInfo << std::endl;
}

void AIDMLog(unsigned int line, std::string errorInfo)
{
	if (line>0)
	{
		std::cout << "AIDM Log @" << line << ": " << errorInfo << std::endl;
	}
	else
	{
		std::cout << "AIDM Log: " << errorInfo << std::endl;
	}
}


