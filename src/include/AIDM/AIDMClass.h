/*
 * AIDMClass.h
 *
 *  Created on: Dec 24, 2015
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_AIDMCLASS_H_
#define INCLUDE_AIDMCLASS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
#include "AIDM/API/AIDMFunctionCall.h"
#include "AIDM/AIDMCondition.h"
#include "AIDM/AIDMAction.h"
#include "AIDM/AIDMClass.h"
#include "AIDM/API/AIDMError.h"

class AIDMClass
{
public:
	std::string _classFile;
	std::string _className;
	AIDMAction* _startGoal;

	std::vector< AIDMAction* > _classActions;
	std::vector< AIDMCondition* > _classConditions;
	std::vector< AIDMFunctionCall* > _classFunctionCalls;

	AIDMClass(std::string classFile);
	bool checkIfActionExists(std::string actionName);
private:
	void Interpret();
};

#endif /* INCLUDE_AIDMCLASS_H_ */
