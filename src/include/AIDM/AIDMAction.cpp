/*
 * AIDMAction.cpp
 *
 *  Created on: Jan 5, 2016
 *      Author: Steffen Kremer
 */

#include <AIDM/MISC/AIDMMisc.h>
#include <AIDM/API/AIDMError.h>
#include "AIDMAction.h"

AIDMAction::AIDMAction(std::string name)
{
	this->_name = name;
	this->_cost = 0;
}

char AIDMAction::AddCondition(std::string str)
{
	str = AIDMMiscClearInterText(str);
	return 0;
}

char AIDMAction::ProcessFunction(std::string str)
{
	//Errors must be >3!!!
	/*
	 * Animation.State("ANIM_Tool_Chop");
	 * ^^^^^^^^^| - read till . --> class name (no . then '(' and --> function)
	 *          |^^^^^| - read till ( --> function
     *                |^^^^^^^^^^^^^^^^| - read till ) and parse parameter
	*/
	std::cout << str << std::endl;
	return 1;
}

char AIDMAction::ProcessString(char type, std::string str)
{
	str = AIDMMiscClearInterText(str);

	/*
	 * --> read ; processfunction(string till ;) then check if next call exists
	 */

	bool bIntInsString = false;
	std::string funcStr = "";
	for (unsigned int uiCurrentChar=0; uiCurrentChar<str.length(); ++uiCurrentChar)
	{
		if (str.at(uiCurrentChar)!=';'&&uiCurrentChar==str.length())
		{
			return 2; // no close ;
		}
		else if (uiCurrentChar==str.length()&&bIntInsString)
		{
			return 3; // inside string on exit
		}
		else if (str.at(uiCurrentChar)=='\"')
		{
			bIntInsString = !bIntInsString;
		}
		else if (str.at(uiCurrentChar)==';')
		{
			if (str.at(uiCurrentChar)==';'&&uiCurrentChar==str.length())
			{
				return 1;
			}
			char cFPr = this->ProcessFunction(funcStr);
			if (cFPr==1)
			{
				AIDMLog(0,"Added Function!");
				funcStr = "";
			}
			else
			{
				return cFPr;
			}
		}
		else
		{
			funcStr += str.at(uiCurrentChar);
		}
	}
	return 0;
}
