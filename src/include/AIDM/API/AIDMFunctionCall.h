/*
 * AIDMFunctionCall.h
 *
 *  Created on: Dec 24, 2015
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_AIDMFUNCTIONCALL_H_
#define INCLUDE_AIDMFUNCTIONCALL_H_

#include <iostream>
#include <vector>

class AIDMFunctionParameter
{
public:
	unsigned char _parType;
	std::string _parValue;
};

class AIDMFunctionCall
{
public:
	std::string _className;
	std::string _functionName;

	std::vector< AIDMFunctionParameter* > _functionParameters;
};

#endif /* INCLUDE_AIDMFUNCTIONCALL_H_ */
