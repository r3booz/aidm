/*
 * AIDMMisc.h
 *
 *  Created on: Jan 8, 2016
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_AIDM_AIDMMISC_H_
#define INCLUDE_AIDM_AIDMMISC_H_

#include <iostream>

bool AIDMMiscCheckLegalChar(unsigned char in);
bool AIDMMiscCheckLegalStr(std::string in);
std::string AIDMMiscClearInterText(std::string in);

#endif /* INCLUDE_AIDM_AIDMMISC_H_ */
