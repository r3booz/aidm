/*
 * AIDMError.h
 *
 *  Created on: Dec 30, 2015
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_AIDM_AIDMERROR_H_
#define INCLUDE_AIDM_AIDMERROR_H_

#include <cstdlib>
#include <iostream>

void AIDMError(int errorNR, unsigned int line, std::string errorInfo);

void AIDMWarning(int errorNR, unsigned int line, std::string errorInfo);

void AIDMLog(unsigned int line, std::string errorInfo);

#endif /* INCLUDE_AIDM_AIDMERROR_H_ */
