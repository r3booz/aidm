/*
 * AIDMAction.h
 *
 *  Created on: Dec 24, 2015
 *      Author: Steffen Kremer
 */

#ifndef INCLUDE_AIDMACTION_H_
#define INCLUDE_AIDMACTION_H_

#include <iostream>
#include <vector>
#include "AIDMCondition.h"
#include "API/AIDMFunctionCall.h"

class AIDMAction
{
public:
	unsigned int _cost;
	std::string _name;
	std::vector< AIDMCondition* > _preCondition;
	std::vector< AIDMFunctionCall* > _effect;
	std::vector< AIDMFunctionCall* > _function;
	std::vector< AIDMCondition* > _fail;

	AIDMAction(std::string name);
	char AddCondition(std::string str);
	char ProcessFunction(std::string str);
	char ProcessString(char type, std::string str);
};

#endif /* INCLUDE_AIDMACTION_H_ */
